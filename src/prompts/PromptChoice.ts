export interface PromptChoice {
	name: string;
	value: string;
	checked: boolean;
}
