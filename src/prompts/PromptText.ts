import * as inquirer from "inquirer";

export function promptForText(message: string, defaultValue?: string): Promise<string> {
	return inquirer
		.prompt({
			type: "input",
			name: "value",
			message: message,
			default: defaultValue,
			validate: (value) => !!value
		})
		.then((response) => response.value);
}
