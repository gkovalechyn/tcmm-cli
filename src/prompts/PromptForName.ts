import * as inquirer from "inquirer";

export function promptForName(defaultValue?: string): Promise<string> {
	return inquirer
		.prompt({
			type: "input",
			name: "repositoryName",
			message: "Repository name",
			default: defaultValue,
			validate: (value) => !!value
		})
		.then((response) => response.repositoryName);
}
