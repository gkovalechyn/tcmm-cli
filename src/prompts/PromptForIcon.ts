import * as inquirer from "inquirer";

export function promptForIcon(defaultValue?: string): Promise<string> {
	return inquirer
		.prompt({
			type: "input",
			name: "iconUrl",
			message: "Repository icon URL",
			default: defaultValue
		})
		.then((response) => response.iconUrl);
}
