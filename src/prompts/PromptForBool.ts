import * as inquirer from "inquirer";

export function promptForBool(message: string, defaultValue?: boolean): Promise<boolean> {
	return inquirer
		.prompt({
			type: "confirm",
			name: "value",
			message: message,
			default: defaultValue
		})
		.then((response) => response.value);
}
