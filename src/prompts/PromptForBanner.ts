import * as inquirer from "inquirer";

export function promptForBanner(defaultValue?: string): Promise<string> {
	return inquirer
		.prompt({
			type: "input",
			name: "bannerUrl",
			message: "Repository banner URL",
			default: defaultValue
		})
		.then((response) => response.bannerUrl);
}
