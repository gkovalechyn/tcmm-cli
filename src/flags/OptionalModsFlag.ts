import { flags } from "@oclif/command";

export const OptionalModsFlag = flags.string({
	char: "o",
	description: "A comma separated list of mods that should be marked as optional mods",
	helpValue: "@mod1,@mod2,@mod3"
});
