import { flags } from "@oclif/command";

export const IconFlag = flags.string({
	char: "i",
	description: "The URL of the icon of the repository",
	helpValue: "https://example.com/icon.png"
});
