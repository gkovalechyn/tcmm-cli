import { flags } from "@oclif/command";

export const BannerFlag = flags.string({
	char: "b",
	description: "The URL of the banner of the repository",
	helpValue: "https://example.com/banner.png"
});
