import { flags } from "@oclif/command";

export const NameFlag = flags.string({
	char: "n",
	description: "The name of the repository",
	helpValue: "My mod repository"
});
