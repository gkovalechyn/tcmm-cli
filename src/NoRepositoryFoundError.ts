import { Constants } from "./Constants";
import { cwd } from "process";

export class NoRepositoryFoundError extends Error {
	public constructor() {
		super(`No ${Constants.REPOSITORY_FILE_NAME} found in ${cwd()}`);
	}
}
