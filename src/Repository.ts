import "reflect-metadata";
import { Server } from "./Server";
import { FileInfo } from "./FileInfo";
import { Type, deserialize, serialize } from "class-transformer";
import { readFileSync, writeFileSync } from "fs";
import { Constants } from "./Constants";
import { cwd } from "process";
import { resolve } from "path";

export class Repository {
	public version = 1;

	public name = "";
	public iconUrl = "";
	public bannerUrl = "";

	@Type(() => Date)
	public generationDate = new Date();

	@Type(() => Server)
	public servers: Server[] = [];

	@Type(() => FileInfo)
	public files: Map<string, FileInfo> = new Map();

	public optionalMods: string[] = [];

	public get Mods(): string[] {
		const modNames = new Set<string>();

		for (const path of this.files.keys()) {
			const modName = path.split(/[\/\\]/g)[0];

			modNames.add(modName);
		}

		return Array.from(modNames);
	}

	public get DisplaySize(): string {
		let size = 0;

		for (const entry of this.files.entries()) {
			size += entry[1].size;
		}

		let i = 0;
		const units = [" bytes", " KiB", " MiB", " GiB", " TiB", "PiB", "EiB", "ZiB", "YiB"];
		while (size > 1024) {
			size = size / 1024;
			i++;
		}

		return size.toFixed(2) + units[i];
	}

	public static loadFromFile(path: string): Repository {
		return deserialize(Repository, readFileSync(path, { encoding: "utf8" }));
	}

	public saveToCurrentDir(): void {
		writeFileSync(resolve(cwd(), Constants.REPOSITORY_FILE_NAME), serialize(this));
	}
}
