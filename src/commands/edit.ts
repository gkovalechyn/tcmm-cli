import { Command } from "@oclif/command";
import { resolve } from "path";
import { cwd } from "process";
import { Constants } from "../Constants";
import { existsSync } from "fs";
import { NameFlag } from "../flags/NameFlag";
import { IconFlag } from "../flags/IconFlag";
import { BannerFlag } from "../flags/BannerFlag";
import { OptionalModsFlag } from "../flags/OptionalModsFlag";
import { Repository } from "../Repository";
import { promptForName } from "../prompts/PromptForName";
import { promptForIcon } from "../prompts/PromptForIcon";
import { promptForBanner } from "../prompts/PromptForBanner";
import { NoRepositoryFoundError } from "../NoRepositoryFoundError";
import { PromptChoice } from "../prompts/PromptChoice";
import * as inquirer from "inquirer";

export default class EditCommand extends Command {
	static description = "Edits an existing repository file";

	static flags = {
		name: NameFlag,
		icon: IconFlag,
		banner: BannerFlag,
		optional: OptionalModsFlag
	};

	public async run(): Promise<void> {
		const { flags } = this.parse(EditCommand);
		const filePath = resolve(cwd(), Constants.REPOSITORY_FILE_NAME);

		if (!existsSync(filePath)) {
			throw new NoRepositoryFoundError();
		}

		const repository = Repository.loadFromFile(filePath);

		if (flags.name) {
			repository.name = flags.name;
		} else {
			repository.name = await promptForName(repository.name);
		}

		if (flags.icon) {
			repository.iconUrl = flags.icon;
		} else {
			repository.iconUrl = await promptForIcon(repository.iconUrl);
		}

		if (flags.banner) {
			repository.bannerUrl = flags.banner;
		} else {
			repository.bannerUrl = await promptForBanner(repository.bannerUrl);
		}

		if (flags.optional) {
			repository.optionalMods = flags.optional.split(",");
		} else {
			const mods = repository.Mods.sort((lhs, rhs) => lhs.localeCompare(rhs));
			const choices = mods.map((modName) => {
				return {
					name: modName,
					value: modName,
					checked: repository.optionalMods.includes(modName)
				} as PromptChoice;
			});

			repository.optionalMods = await inquirer
				.prompt({
					type: "checkbox",
					name: "optionalMods",
					message: "Optional mods",
					choices: choices
				})
				.then((response) => response.optionalMods as string[]);
		}

		this.log("Changes applied successfuly");
	}
}
