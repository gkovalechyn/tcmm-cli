import { Command } from "@oclif/command";
import { resolve } from "path";
import { cwd } from "process";
import { Constants } from "../Constants";
import { existsSync } from "fs";
import { NoRepositoryFoundError } from "../NoRepositoryFoundError";
import Listr, { ListrTask } from "listr";
import axios from "axios";
import { Repository } from "../Repository";
import { plainToClass } from "class-transformer";

function CreateTestRangeHeaderTask(url: string): Promise<void> {
	return new Promise((resolve, reject) => {
		axios
			.head(url, {
				headers: {
					Range: "0"
				}
			})
			.then((response) => {
				if (response.headers["accept-ranges"] === "bytes") {
					resolve();
				} else {
					reject(new Error("Http server does not accept ranged requests, download pausing and resuming will not work"));
				}
			})
			.catch((error) => {
				reject(error);
			});
	});
}

function CreateUrlReturnsRepositoryTask(url: string): Promise<void> {
	return new Promise((resolve, reject) => {
		axios
			.get(url)
			.then((response) => {
				const jsonReponse = response.data;

				if (
					jsonReponse.name != null &&
					jsonReponse.iconUrl != null &&
					jsonReponse.bannerUrl != null &&
					jsonReponse.generationDate != null &&
					jsonReponse.servers != null &&
					jsonReponse.files != null &&
					jsonReponse.optionalMods != null
				) {
					resolve();
				} else {
					reject(new Error("File returned by the server is not a repository file"));
				}
			})
			.catch((error) => reject(error));
	});
}

function CreateRepositoryMatchesTask(url: string, repository: Repository): Promise<void> {
	return new Promise((resolve, reject) => {
		axios
			.get(url)
			.then((response) => {
				const responseRepository = plainToClass(Repository, response.data);
				const remoteMods = responseRepository.Mods;
				const differentMods = repository.Mods.filter((modName) => !remoteMods.includes(modName));

				if (responseRepository.name === repository.name && differentMods.length === 0) {
					resolve();
				} else {
					reject(new Error("Remote repository and local repository do not match"));
				}
			})
			.catch((error) => {
				reject(error);
			});
	});
}

export class TestCommand extends Command {
	static description =
		"Makes a call to the given URL and tests wether it returns the repository in the current working directory";

	static args = [
		{
			name: "url",
			required: true,
			description: "The URL of the repository to test for"
		}
	];

	public async run(): Promise<void> {
		const { args } = this.parse(TestCommand);
		const repositoryPath = resolve(cwd(), Constants.REPOSITORY_FILE_NAME);
		let repository: Repository | undefined;

		if (!existsSync(repositoryPath)) {
			this.warn(new NoRepositoryFoundError());
			this.warn("Continuing anyway");
		} else {
			repository = Repository.loadFromFile(repositoryPath);
		}

		const repositoryFileUrl = args.url + "/" + Constants.REPOSITORY_FILE_NAME;
		this.log("Testing url: " + repositoryFileUrl);

		const tasks: ListrTask[] = [
			{
				title: "URL returns a repository file",
				task: (): Promise<void> => CreateUrlReturnsRepositoryTask(repositoryFileUrl)
			},
			{
				title: "Http server accepts ranged requests",
				task: (): Promise<void> => CreateTestRangeHeaderTask(repositoryFileUrl)
			}
		];

		if (repository) {
			tasks.push({
				title: "URL repository matches the repository file in the current directory",
				task: (): Promise<void> => CreateRepositoryMatchesTask(repositoryFileUrl, repository!)
			});
		}

		const listr = new Listr(tasks, {
			concurrent: true,
			exitOnError: false
		});

		await listr.run();
	}
}
