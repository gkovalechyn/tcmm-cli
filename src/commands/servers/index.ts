import { Command } from "@oclif/command";

export default class ServersCommand extends Command {
	static description = "Manages the servers for the repository in the current folder";

	public async run(): Promise<void> {
		this._help();
	}
}
