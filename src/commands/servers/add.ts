import { Command } from "@oclif/command";
import { resolve } from "path";
import { cwd } from "process";
import { Constants } from "../../Constants";
import { existsSync } from "fs";
import { NoRepositoryFoundError } from "../../NoRepositoryFoundError";
import { Repository } from "../../Repository";
import { Server } from "../../Server";
import { promptForText } from "../../prompts/PromptText";
import { promptForBool } from "../../prompts/PromptForBool";

export default class ServersAddCommand extends Command {
	static description = "Adds a new server to the list of servers in the repository file";

	public async run(): Promise<void> {
		const filePath = resolve(cwd(), Constants.REPOSITORY_FILE_NAME);

		if (!existsSync(filePath)) {
			throw new NoRepositoryFoundError();
		}

		const repository = Repository.loadFromFile(filePath);

		const server = new Server();

		server.name = await await promptForText("Server name");

		server.url = await promptForText("Server URL");

		server.requiresPassword = await promptForBool("Requires password", true);

		if (server.requiresPassword) {
			server.password = await promptForText("Server password");
		}

		repository.servers.push(server);
		repository.saveToCurrentDir();

		this.log("Saved server to repository file");
	}
}
