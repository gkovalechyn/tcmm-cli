import { Command } from "@oclif/command";
import { resolve } from "path";
import { cwd } from "process";
import { Constants } from "../../Constants";
import { existsSync } from "fs";
import { NoRepositoryFoundError } from "../../NoRepositoryFoundError";
import { Repository } from "../../Repository";

export default class ServersDeleteCommand extends Command {
	static description = "Deletes a server from list of servers in the repository file";

	static args = [
		{
			name: "serverIndex",
			required: true,
			description: "The index of the server to be deleted"
		}
	];

	public async run(): Promise<void> {
		const { args } = this.parse(ServersDeleteCommand);

		const filePath = resolve(cwd(), Constants.REPOSITORY_FILE_NAME);

		if (!existsSync(filePath)) {
			throw new NoRepositoryFoundError();
		}

		const repository = Repository.loadFromFile(filePath);
		const index = parseInt(args.serverIndex);

		if (index < 0 || index >= repository.servers.length) {
			return this.error(`Index must be between 0 and ${repository.servers.length}`);
		}

		repository.servers.splice(index, 1);
		repository.saveToCurrentDir();

		this.log("Server deleted from list");
	}
}
