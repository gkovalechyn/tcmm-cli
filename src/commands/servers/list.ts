import { Command } from "@oclif/command";
import { resolve } from "path";
import { cwd } from "process";
import { Constants } from "../../Constants";
import { existsSync } from "fs";
import { NoRepositoryFoundError } from "../../NoRepositoryFoundError";
import { Repository } from "../../Repository";
import cli from "cli-ux";
import { Server } from "../../Server";

type TableRow = {
	index: number;
	server: Server;
};

export default class ServersListCommand extends Command {
	static description = "Lists the servers in the current repository file";

	public async run(): Promise<void> {
		const filePath = resolve(cwd(), Constants.REPOSITORY_FILE_NAME);

		if (!existsSync(filePath)) {
			throw new NoRepositoryFoundError();
		}

		const repository = Repository.loadFromFile(filePath);

		const data: TableRow[] = [];

		let i = 0;
		for (const server of repository.servers) {
			data.push({
				index: i++,
				server: server
			});
		}

		cli.table(data, {
			index: {
				header: "Index",
				get: (row): number => row.index
			},
			name: {
				get: (row): string => row.server.name
			},
			url: {
				get: (row): string => row.server.url
			},
			requiresPassword: {
				header: "Requires password?",
				get: (row): boolean => row.server.requiresPassword
			},
			password: {
				get: (row): string => row.server.password
			}
		});
	}
}
