import { Command } from "@oclif/command";
import { cwd } from "process";
import { Repository } from "../Repository";
import { resolve } from "path";
import { Constants } from "../Constants";
import { existsSync, readFileSync } from "fs";
import { deserialize } from "class-transformer";
import * as inquirer from "inquirer";
import Listr from "listr";
import { GenerateRepositoryTask } from "../tasks/GenerateRepositoryTask";
import { TaskContext } from "../TaskContext";
import { Observable } from "rxjs";
import { NameFlag } from "../flags/NameFlag";
import { IconFlag } from "../flags/IconFlag";
import { BannerFlag } from "../flags/BannerFlag";
import { OptionalModsFlag } from "../flags/OptionalModsFlag";
import { promptForName } from "../prompts/PromptForName";
import { promptForIcon } from "../prompts/PromptForIcon";
import { promptForBanner } from "../prompts/PromptForBanner";

export default class GenerateCommand extends Command {
	static description = "Generates the repository file from the mods in the current working directory";

	static flags = {
		name: NameFlag,
		icon: IconFlag,
		banner: BannerFlag,
		optional: OptionalModsFlag
	};

	static args = [];

	public async run(): Promise<void> {
		const { flags } = this.parse(GenerateCommand);
		const repositoryFolder = cwd();
		const repositoryFilePath = resolve(repositoryFolder, Constants.REPOSITORY_FILE_NAME);
		let repository = new Repository();

		if (existsSync(repositoryFilePath)) {
			repository = deserialize(Repository, readFileSync(repositoryFilePath, { encoding: "utf8" }));
		}

		if (flags.name) {
			repository.name = flags.name;
		} else {
			repository.name = await promptForName(repository.name);
		}

		if (flags.icon) {
			repository.iconUrl = flags.icon;
		} else {
			repository.iconUrl = await promptForIcon(repository.iconUrl);
		}

		if (flags.banner) {
			repository.bannerUrl = flags.banner;
		} else {
			repository.bannerUrl = await promptForBanner(repository.bannerUrl);
		}

		const context = new TaskContext();

		const tasks = new Listr([
			{
				title: "Calculating hashes",
				task: (): Observable<string> => GenerateRepositoryTask(repositoryFolder, context)
			}
		]);

		await tasks.run();
		repository.files = context.output;
		repository.generationDate = new Date();

		this.log(`Generated hashes for ${repository.files.size} files with a total size of ${repository.DisplaySize}`);

		if (flags.optional) {
			repository.optionalMods = flags.optional.split(",");
		} else {
			await this.selectOptionalMods(repository);
		}

		repository.saveToCurrentDir();

		this.log("Repository file created successfuly");
	}

	private selectOptionalMods(repository: Repository): Promise<void> {
		const mods = repository.Mods;
		mods.sort((lhs, rhs) => lhs.localeCompare(rhs));

		const choices = mods.map((name) => {
			return {
				name: name,
				value: name,
				checked: repository.optionalMods.includes(name)
			};
		});

		return inquirer
			.prompt({
				type: "checkbox",
				name: "optionalMods",
				message: "Optional mods",
				choices: choices
			})
			.then((response) => {
				repository.optionalMods = response.optionalMods as string[];
			});
	}
}
