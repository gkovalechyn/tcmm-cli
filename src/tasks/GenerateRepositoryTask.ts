import { FileInfo } from "../FileInfo";
import { readdirSync, createReadStream, statSync } from "fs";
import { resolve } from "path";
import { createHash } from "crypto";
import { TaskContext } from "../TaskContext";
import { Observable, Subscriber } from "rxjs";
import { Constants } from "../Constants";

async function calculateHash(path: string): Promise<string> {
	return new Promise<string>((resolve, reject) => {
		const fileStream = createReadStream(path, { autoClose: true });
		const md5 = createHash("md5");

		md5.setEncoding("hex");

		fileStream.on("data", (data) => {
			md5.update(data);
		});

		fileStream.on("end", () => {
			resolve(md5.digest("hex"));
		});

		fileStream.on("error", (error) => {
			reject(error);
		});
	});
}

async function scanFolder(
	folderPath: string,
	relativePath: string,
	context: TaskContext,
	subscriber: Subscriber<string>
): Promise<void> {
	const entries = readdirSync(folderPath, { withFileTypes: true });

	for (const entry of entries) {
		if (entry.isDirectory()) {
			const nestedFolderPath = resolve(folderPath, entry.name);
			await scanFolder(nestedFolderPath, relativePath + "/" + entry.name, context, subscriber);
		} else {
			// Ignore the repository file
			if (entry.name === Constants.REPOSITORY_FILE_NAME) {
				continue;
			}

			subscriber.next(entry.name);

			const filePath = resolve(folderPath, entry.name);
			const info = new FileInfo();
			info.name = entry.name;
			info.hash = await calculateHash(filePath);
			info.size = statSync(filePath).size;

			// Setup file relative path and remove the first "/" character
			const fullRelativePath = `${relativePath}/${entry.name}`.slice(1);

			context.output.set(fullRelativePath, info);
		}
	}
}

export function GenerateRepositoryTask(path: string, context: TaskContext): Observable<string> {
	return new Observable<string>((observer) => {
		scanFolder(path, "", context, observer).then(() => {
			observer.complete();
		});
	});
}
