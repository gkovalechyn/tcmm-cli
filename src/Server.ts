export class Server {
	public version = 1;

	public name = "";
	public url = "";
	public requiresPassword = false;
	public password = "";
}
