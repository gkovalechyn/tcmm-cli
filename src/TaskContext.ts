import { FileInfo } from "./FileInfo";

export class TaskContext {
	public output = new Map<string, FileInfo>();
}
