tcmm-cli
========

Repository manager cli for The Cooler Server

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/tcmm-cli.svg)](https://npmjs.org/package/tcmm-cli)
[![Downloads/week](https://img.shields.io/npm/dw/tcmm-cli.svg)](https://npmjs.org/package/tcmm-cli)
[![License](https://img.shields.io/npm/l/tcmm-cli.svg)](https://github.com/gkovalechyn/tcmm-cli/blob/master/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g tcmm-cli
$ tcmm-cli COMMAND
running command...
$ tcmm-cli (-v|--version|version)
tcmm-cli/1.1.0 win32-x64 node-v14.8.0
$ tcmm-cli --help [COMMAND]
USAGE
  $ tcmm-cli COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`tcmm-cli edit`](#tcmm-cli-edit)
* [`tcmm-cli generate`](#tcmm-cli-generate)
* [`tcmm-cli help [COMMAND]`](#tcmm-cli-help-command)
* [`tcmm-cli servers`](#tcmm-cli-servers)
* [`tcmm-cli servers:add`](#tcmm-cli-serversadd)
* [`tcmm-cli servers:delete SERVERINDEX`](#tcmm-cli-serversdelete-serverindex)
* [`tcmm-cli servers:list`](#tcmm-cli-serverslist)
* [`tcmm-cli test URL`](#tcmm-cli-test-url)

## `tcmm-cli edit`

Edits an existing repository file

```
USAGE
  $ tcmm-cli edit

OPTIONS
  -b, --banner=https://example.com/banner.png  The URL of the banner of the repository
  -i, --icon=https://example.com/icon.png      The URL of the icon of the repository
  -n, --name=My mod repository                 The name of the repository
  -o, --optional=@mod1,@mod2,@mod3             A comma separated list of mods that should be marked as optional mods
```

_See code: [src/commands/edit.ts](https://github.com/gkovalechyn/tcmm-cli/blob/v1.1.0/src/commands/edit.ts)_

## `tcmm-cli generate`

Generates the repository file from the mods in the current working directory

```
USAGE
  $ tcmm-cli generate

OPTIONS
  -b, --banner=https://example.com/banner.png  The URL of the banner of the repository
  -i, --icon=https://example.com/icon.png      The URL of the icon of the repository
  -n, --name=My mod repository                 The name of the repository
  -o, --optional=@mod1,@mod2,@mod3             A comma separated list of mods that should be marked as optional mods
```

_See code: [src/commands/generate.ts](https://github.com/gkovalechyn/tcmm-cli/blob/v1.1.0/src/commands/generate.ts)_

## `tcmm-cli help [COMMAND]`

display help for tcmm-cli

```
USAGE
  $ tcmm-cli help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v3.2.2/src/commands/help.ts)_

## `tcmm-cli servers`

Manages the servers for the repository in the current folder

```
USAGE
  $ tcmm-cli servers
```

_See code: [src/commands/servers/index.ts](https://github.com/gkovalechyn/tcmm-cli/blob/v1.1.0/src/commands/servers/index.ts)_

## `tcmm-cli servers:add`

Adds a new server to the list of servers in the repository file

```
USAGE
  $ tcmm-cli servers:add
```

_See code: [src/commands/servers/add.ts](https://github.com/gkovalechyn/tcmm-cli/blob/v1.1.0/src/commands/servers/add.ts)_

## `tcmm-cli servers:delete SERVERINDEX`

Deletes a server from list of servers in the repository file

```
USAGE
  $ tcmm-cli servers:delete SERVERINDEX

ARGUMENTS
  SERVERINDEX  The index of the server to be deleted
```

_See code: [src/commands/servers/delete.ts](https://github.com/gkovalechyn/tcmm-cli/blob/v1.1.0/src/commands/servers/delete.ts)_

## `tcmm-cli servers:list`

Lists the servers in the current repository file

```
USAGE
  $ tcmm-cli servers:list
```

_See code: [src/commands/servers/list.ts](https://github.com/gkovalechyn/tcmm-cli/blob/v1.1.0/src/commands/servers/list.ts)_

## `tcmm-cli test URL`

Makes a call to the given URL and tests wether it returns the repository in the current working directory

```
USAGE
  $ tcmm-cli test URL

ARGUMENTS
  URL  The URL of the repository to test for
```

_See code: [src/commands/test.ts](https://github.com/gkovalechyn/tcmm-cli/blob/v1.1.0/src/commands/test.ts)_
<!-- commandsstop -->
